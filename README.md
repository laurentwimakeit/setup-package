![Logo](https://www.wimakeit.be/app/logos/wimakeit.svg)


# Magento 2.3+ Setup Package

Magento2 composer meta package setup




[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/) [![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/) [![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)


## Features

- baldwin/magento2-module-url-data-integrity-checker 
- cweagans/composer-patches
- dompdf/dompdf"
- element119/module-cms-duplicator
- experius/module-pagenotfound
- experius/module-wysiwygdownloads
- facebook/facebook-for-magento2
- google/apiclient
- icamys/php-sitemap-generator
- magento-hackathon/module-eavcleaner-m2
- magepal/magento2-gmailsmtpapp,
- magepal/magento2-googletagmanager
- mailjet/mailjet-apiv3-php
- msp/devtools
- multisafepay/magento2
- owebia/magento2-module-advanced-shipping
- shkoliar/magento-grid-colors
- yireo/magento2-webp2
- pragmatic-modules/magento2-module-system-configuration-toolkit

#### Suggested Modules : 
- wimakeit/og-tag-manager
- wimakeit/core

## Installation

Install my-project with npm

```bash
  composer require wimakeit/setup-package
  bin/magento setup:upgrade
  bin/magento setup:di:compile  
```
    
## Authors

- [@emetik](https://www.github.com/emetik)


## Support

For support, email contact@wimakeit.com

